/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#include "Shader.h"


Shader::Shader(const char *vertexFile, const char *fragmentFile)
{
	try
	{
		GLchar *vertexShader = readFile(vertexFile);
		GLchar *fragShader = readFile(fragmentFile);
		setVertexShader(vertexShader);
		setFragmentShader(fragShader);

	}
	catch (out_of_range e)
	{
		cout << e.what() << endl;

	}

}

GLchar * Shader::readFile(const char *fileName)
{
	ifstream file;

	long long int fileLength, endPosition;

	file.open(fileName, ios::binary);

	if (file.is_open())
	{
		endPosition = file.tellg();
		file.seekg(0, ios::end);
		fileLength = (long long int)file.tellg() - endPosition;
		file.seekg(0, ios::beg);
		GLchar *fileSource  = new char[fileLength + 1];
		file.read(fileSource, fileLength);
		fileSource[fileLength] = '\0';
		file.close();

		//delete[] fileSource;
		return fileSource;

	}
	else
	{
		throw out_of_range("File Cannot Be opened");
	}
}
bool Shader:: generateProgram()
{
	//Crate shader program
	shaderProgram = glCreateProgram();
	bind();
	glLinkProgram(shaderProgram);
	char buffer[500];
	GLint status;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);
	if (!status)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, buffer);
		return false;
	}
	return true;
}
bool Shader::createShader()
{

	if (!generateVertexShader())
	{
		return false;
	}

	if (!generateFragmentShader())
	{
		return false;
	}

	if (!generateProgram())
	{
		return false;
	}
	unload();
	return true;
}
void Shader::unload()
{

	glDeleteShader(vShader);
	glDeleteShader(fShader);
	vShader = 0;
	fShader = 0;
}
void Shader::bind()
{
	glAttachShader(shaderProgram, vShader);
	glAttachShader(shaderProgram, fShader);
}
bool Shader::generateVertexShader()
{
	const char* shader = getVertexShader();
	char buffer[512];
	// Vertex Shader Code Generation
	vShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vShader, 1, &shader, NULL);
	glCompileShader(vShader);

	GLint status;
	glGetShaderiv(vShader, GL_COMPILE_STATUS, &status);
	if (!status == GL_TRUE)
	{
		glGetShaderInfoLog(vShader, 512, NULL, buffer);
		cout << buffer << endl;
		return false;
	}
	return true;
}


bool Shader::generateFragmentShader()
{
	const char* shader = getFragmentShader();
	char buffer[512];
	GLint status;

	fShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fShader, 1, &shader, NULL);
	glCompileShader(fShader);

	glGetShaderiv(fShader, GL_COMPILE_STATUS, &status);
	if (!status == GL_TRUE)
	{
		glGetShaderInfoLog(fShader, 512, NULL, buffer);
		cout << buffer << endl;
		return false;

	}

	return true;
}
void Shader::setVertexShader(const GLchar *vertexShader)
{
	this->vertexShader = vertexShader;
}
const char* Shader::getVertexShader()
{
	return vertexShader;
}
void Shader::setFragmentShader(const GLchar *fragmentShader)
{
	this->fragmentShader = fragmentShader;

}
const char* Shader::getFragmentShader()
{
	return fragmentShader;
}

GLuint Shader::use()
{
	return shaderProgram;
}
void Shader:: useProgram()
{
	glUseProgram(shaderProgram);
}

void Shader::sendUniformfMat4by4(const char* name, glm::mat4 trans)
{
	//send information to shader for mat4by4 uniform
	transLocation = glGetUniformLocation(shaderProgram, name);
	glUniformMatrix4fv(transLocation, 1, GL_FALSE, glm::value_ptr(trans));
}

void Shader::sendUniformfvec3(const char* name, glm::vec3 trans)
{
	transLocation = glGetUniformLocation(shaderProgram, name);
	glUniform3f(transLocation, trans.x, trans.y, trans.z);
}

void Shader::sendFloat(const char*name, float trans)
{
	transLocation = glGetUniformLocation(shaderProgram, name);
	glUniform1f(transLocation, trans);
}
void Shader::sendInt(const char* name, int trans)
{
	transLocation = glGetUniformLocation(shaderProgram, name);
	glUniform1i(transLocation, trans);
}
Shader::~Shader()
{
	unload();
}
