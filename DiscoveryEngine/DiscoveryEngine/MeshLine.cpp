/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#include "MeshLine.h"

void MeshLine::setupMesh(int verticies)
{
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, mySize*4,verts , GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, verticies * sizeof(float), (void*)0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, verticies * sizeof(float), (void*)(3 * sizeof(float)));

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, verticies * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}
MeshLine::MeshLine()
{

}
MeshLine::MeshLine(float *vertexData, int verticies, int size)
{
	this->verts = new float[size];

	for (int i = 0; i < size; i++)
	{
		//std::cout << verts[i]<< std::endl;
		this->verts[i] = vertexData[i];
	}
	mySize = size;
	setupMesh(verticies);

}
void MeshLine::Draw(GLenum drawType, Shader* shader,glm::mat4 projection,
	Camera* camera, int count)
{
	shader->useProgram();
	camera->setShader(shader);
	shader->sendUniformfMat4by4("projection", projection);
	camera->Update();
	glm::mat4 model;
	shader->sendUniformfMat4by4("model", model);
	glBindVertexArray(vao);
	glDrawArrays(drawType,0, count);

}