#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
class GBuffer {

private:
	unsigned int gBuffer, gPostion, gNormal, gColorSpec,specular,finalT;
	float width, height;
	unsigned int * attachments;
	unsigned int depth;

public:
	GBuffer(float width, float height);
	~GBuffer();
	unsigned int getGbuffer();
	void createFramebuffer();
	void renderFramebuffer();
	void renderForGeometryPass();

	void renderTextures();
	void unbind();
};