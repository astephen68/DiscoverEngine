
/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/


#pragma once
#include "Mesh.h"
#include <map>


#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define MAX_BONES 200

class SkinnedMesh:public Mesh
{
public:
	SkinnedMesh();
	~SkinnedMesh();

	float time;
	float start_time;
	float end_time;
	float u = 0.0f;


	Animation* active_animation;
	Animation* idle_animation;

	bool anim_play;
	bool anim_loop;

	float frameTime;
	void PlayAnimation(Animation& anim, bool loop, bool reset_to_start);
	void StopAnimating();
	void Update(float currentFrame);

	void Update(float currentFrame, std::vector <glm::mat4> trans);
	void SetIdleAnimation(Animation* in_anim);
	
	std::vector<glm::mat4> Transforms;
	bool LoadMesh(const string& FileName);

	void Render();

	unsigned int NumBones() const
	{
		return m_NumBones;
	}

	void BoneTransform(float TimeInSeconds, std::vector<glm::mat4>& transforms);
	void SetBoneTransform(unsigned int Index, const glm::mat4 &Transform)
	{
		assert(Index < MAX_BONES);
		//Transform.Print();
		glUniformMatrix4fv(Index,1, GL_FALSE,(const GLfloat*)&Transform);
	}


private:
	#define NUM_BONES_PER_VERTEX 4

	GLuint m_boneLocation[MAX_BONES];
	struct BoneInfo
	{
		glm::mat4 BoneOffset;
		glm::mat4 FinalTransformation;
	};

	struct VertexBoneData
	{
		unsigned int IDs[NUM_BONES_PER_VERTEX];
		float Weights[NUM_BONES_PER_VERTEX];

		VertexBoneData()
		{
			Reset();
		}

		void Reset()
		{
			ZERO_MEM(IDs);
			ZERO_MEM(Weights);
		}

		void AddBoneData(unsigned int BoneID, float Weight);
	};

	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim *pNodeAnim);
	void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const std::string NodeName);
	void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const glm::mat4 ParentTransform);
	bool InitFromScene(const aiScene* pScene, const string& Filename);
	void InitMesh(unsigned int MeshIndex,
		const aiMesh* paiMesh,
		std::vector<glm::vec3>& Positions,
		std::vector<glm::vec3>& Normals,
		std::vector<glm::vec2>& TexCoords,
		std::vector<VertexBoneData>& Bones,
		std::vector<unsigned int>& Indices);
	void LoadBones(unsigned int MeshIndex, const aiMesh* paiMesh, std::vector<VertexBoneData>& Bones);
	bool InitMaterials(const aiScene* pScene, const string& Filename);
	void Clear();

enum VB_TYPES {
	INDEX_BUFFER,
	POS_VB,
	NORMAL_VB,
	TEXCOORD_VB,
	BONE_VB,
	NUM_VBs
};
	GLuint m_VAO;
	GLuint m_buffers[NUM_VBs];

	struct SkinnedMeshEntry {
		SkinnedMeshEntry()
		{
			NumIndices = 0;
			BaseVertex = 0;
			BaseIndex = 0;
			MaterialIndex = INVALID_MATERIAL;
		}

		unsigned int NumIndices;
		unsigned int BaseVertex;
		unsigned int BaseIndex;
		unsigned int MaterialIndex;
	};
	std::vector<SkinnedMeshEntry> m_Entries;
	std::vector<Texture*> m_Textures;
	std::map<std::string, unsigned int> m_BoneMapping;
	unsigned int m_NumBones;
	std::vector<BoneInfo> m_BoneInfo;
	glm::mat4 m_GlobalInverseTransform;

	const aiScene* m_pScene;
	Assimp::Importer m_Importer;

};