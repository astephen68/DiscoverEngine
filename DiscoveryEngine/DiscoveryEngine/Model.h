/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include <iostream>
#include<vector>
#include <string>
#include "Mesh.h"
#include "Camera.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_access.hpp>
#include "Bone.h"

class Model {
private:
	Mesh * myMesh;
	//old stuff to be deleted 
	glm::vec3 pos = glm::vec3(0.0f);
	glm::mat4 modelView = glm::mat4(1.0f);
	glm::mat4 rotation = glm::mat4(1.0f);
	glm::mat4 transform;
	float scale=1.0f;
	float rotx=0.0f,roty=0.0f,rotz=0.0f;
	float transx=0.0f,transy=0.0f,transz=0.0f;
	float max=0.0f;
	float min=0.0f;

public:
	Model(Mesh* myMesh);
	~Model();
	//Skeleton sceneLoaderSkeleton;
	void setPosition(glm::vec3 pos);
	glm::vec3 getPosition();
	float getScale();
	void setScale(float scale);
	void setRotation(float x, float y, float z);
	void setTranslation(float x, float y, float z);
	void Draw();
	void Update(Shader* myShader);

};