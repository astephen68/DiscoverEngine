/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/


#pragma once
#include <glm/glm.hpp>
#include <string>

static float TimeToFrame(float time)
{
	float frame = 0.041666666667;
	return time / frame;
}

static glm::vec2 FramesToTime(glm::vec2 frames)
{
	float frame = 0.041666666667;
	return frames * frame;
}


class Animation
{
public:
	std::string name;
	float start_time;
	float end_time;
	int priority;

	Animation()
	{
		start_time = end_time = priority = 0;
	}

	Animation(std::string in_name, glm::vec2 times, int in_priority)
	{
		name = in_name;
		start_time = times.x;
		end_time = times.y;
		priority = in_priority;
	}
};