/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#define STB_IMAGE_IMPLEMENTATION
#include <iostream>
#include <vector>
class Texture {

private:
	int width=0, height=0, nrChannels=0;
	float *lut=nullptr;
	GLenum wrap=GL_REPEAT, filter=GL_LINEAR;
	std::vector<std::string> textures;
	unsigned int texture=0;
	const char* fileName;
	//loaded texture handle
	unsigned char* myTexture;
	int textureUnit = 0;
	//private methods to load textures and bind
	bool LoadTexture();
public:
	Texture();
	Texture(const char* fileName, GLenum wrap, GLenum filter,int textureUnit);
	~Texture();
	void addTexture(const char*fileName);
	unsigned int createCubeMap();
	bool createTexture();
	bool create1DTexture();
	bool create3DTexture(const char * fileName);
	void bind(int slot = 0);
	void unbind();
	unsigned int getId();
	void update();
	void unload();
	float lutsize = 0.0f;


};