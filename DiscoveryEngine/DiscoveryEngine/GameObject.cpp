/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/


#include "GameObject.h"
#include <iostream>


GameObject::GameObject(SkinnedMesh * myModel)
{
	anim = false;
	model = myModel; 
}

void GameObject::Draw()
{
	if (anim)
	{
	
		model->Render();
	}
}
void GameObject::UpdateSkeleton(float currentFrame)
{
	if (blend==true)
	{
		blendedModel->UpdateSkeleton(currentFrame);
		model->Update(currentFrame,blendedModel->model->Transforms);

	}
	else
	{
		model->Update(currentFrame);
	}
}

void GameObject::AddAnimation(Animation& in_anim)
{
	animations.push_back(in_anim);
}

Animation* GameObject::FindAnimation(std::string anim_to_find)
{
	for (int i = 0; i < animations.size(); i++)
	{
		if (animations.at(i).name==anim_to_find)
		{
			return &animations.at(i);
		}
	}
	return nullptr;
}

void GameObject::PlayAnimation(Animation &anim, bool loop, bool reset_to_start)
{
	model->PlayAnimation(anim, loop, reset_to_start);
}

void GameObject::StopAnimating()
{
	model->StopAnimating();
}