
/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/


#include "BlendedGameObject.h"

BlendedGameObject::BlendedGameObject(GameObject * first, GameObject* second)
{
	ObjectsToBlend.push_back(first);
	ObjectsToBlend.push_back(second);
	newMesh = new SkinnedMesh();
}
BlendedGameObject::~BlendedGameObject()
{

}

void BlendedGameObject::Update(float currentFrame)
{
	
	std::vector<glm::mat4> newTransforms = ObjectsToBlend[0]->model->Transforms;
	std::vector<glm::mat4> transforms;
	for (int i = 0; i < ObjectsToBlend.size(); i++)
	{
		ObjectsToBlend[i]->UpdateSkeleton(currentFrame);
	}


	for (int i = 1; i < ObjectsToBlend.size(); i++)
	{
		for (int j = 0; j < newTransforms.size(); j++)
		{
			transforms.push_back(newTransforms[j] * ObjectsToBlend[i]->model->Transforms[j]);
		}
		newTransforms = ObjectsToBlend[i]->model->Transforms;
	}
	newMesh->Transforms = transforms;
	newMesh->Update(currentFrame);
}
void BlendedGameObject::AddGameObject(GameObject * gameObject)
{
	ObjectsToBlend.push_back(gameObject);
}
void BlendedGameObject::Draw(float currentFrame)
{
	newMesh->Update(currentFrame);
	newMesh->Render();
}

void BlendedGameObject::AddAnimation(Animation& in_anim)
{
	animations.push_back(in_anim);
}

Animation* BlendedGameObject::FindAnimation(std::string anim_to_find)
{
	for (int i = 0; i < animations.size(); i++)
	{
		if (animations.at(i).name == anim_to_find)
		{
			return &animations.at(i);
		}
	}
	return nullptr;
}

void BlendedGameObject::PlayAnimation(Animation &anim, bool loop, bool reset_to_start)
{
	newMesh->PlayAnimation(anim, loop, reset_to_start);
}

void BlendedGameObject::StopAnimating()
{
	newMesh->StopAnimating();
}