/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#include "Light.h"

Light::Light(Shader* shader, glm::vec3 ambient, glm::vec3 diffuse,
	glm::vec3 specular,glm::vec3 l, float shiny, float constant,
	float linear, float quadratic)
{
	setShader(shader);
	setAmbient(ambient);
	setDiffuse(diffuse);
	setSpecular(specular);
	setShiny(shiny);
	setConstant(constant);
	setLinear(linear);
	setQuadratic(quadratic);
	rim = l;
}
void Light::setShader(Shader* shader)
{
	myShader = shader;
}
Shader* Light::getShader()
{
	return myShader;
}
void Light::setAmbient(glm::vec3 amb)
{
	this->ambient = amb;
}
glm::vec3 Light::getAmbient()
{
	return ambient;
}
void Light::setDiffuse(glm::vec3 diffuse)
{
	this->diffuse = diffuse;
}
glm::vec3 Light::getDiffuse()
{
	return diffuse;
}
void Light::setSpecular(glm::vec3 specular)
{
	this->specular = specular;
}
glm::vec3 Light::getSpecular()
{
	return specular;
}

void Light::setShiny(float shiny)
{
	this->shiny = shiny;
}
float Light::getShiny()
{
	return shiny;
}
void Light::setConstant(float constant)
{
	this->constant = constant;
}
float Light::getConstant()
{
	return constant;
}
void Light::setLinear(float linear)
{
	this->linear = linear;
}
float Light::getLinear()
{
	return linear;
}
void Light::setQuadratic(float quad)
{
	quadratic = quad;
}
float Light::getQuadratic()
{
	return quadratic;
}
void Light::sendLight(glm::vec3 position, glm::vec3 dir, int pos)
{

		std::string post = "lights[" + std::to_string(pos) + "].lightPos";
		std::string amb = "lights[" + std::to_string(pos) + "].ambient";
		std::string dif = "lights[" + std::to_string(pos) + "].diffuse";
		std::string spec = "lights[" + std::to_string(pos) + "].specular";
		std::string shy = "lights[" + std::to_string(pos) + "].shiny";
		std::string ri = "lights[" + std::to_string(pos) + "].rim";
		std::string con = "lights[" + std::to_string(pos) + "].constant";
		std::string lin = "lights[" + std::to_string(pos) + "].linear";
		std::string quad = "lights[" + std::to_string(pos) + "].quadratic";


		const char * positions = post.c_str();
		const char * ambient = amb.c_str();
		const char * diffuse = dif.c_str();
		const char * specular = spec.c_str();
		const char* shiny = shy.c_str();
		const char * constant = con.c_str();
		const char * linear = lin.c_str();
		const char * quadratic = quad.c_str();
		const char * rimLight = ri.c_str();


		myShader->useProgram();
	
		myShader->sendUniformfvec3(positions, position);
		//myShader->sendUniformfvec3("dirLight.direction", dir);
		//myShader->sendUniformfvec3("dirLight.ambient", getAmbient());
		//myShader->sendUniformfvec3("dirLight.diffuse", getDiffuse());
		//myShader->sendUniformfvec3("dirLight.specular", getSpecular());
		myShader->sendUniformfvec3(ambient, getAmbient());
		myShader->sendUniformfvec3(diffuse, getDiffuse());
		myShader->sendUniformfvec3(specular, getSpecular());
		myShader->sendUniformfvec3(rimLight, rim);

		myShader->sendFloat(shiny, getShiny());
		myShader->sendFloat(constant, getConstant());
		myShader->sendFloat(linear, getLinear());
		myShader->sendFloat(quadratic,getQuadratic());

}

Light::~Light()
{
	myShader = nullptr;
}