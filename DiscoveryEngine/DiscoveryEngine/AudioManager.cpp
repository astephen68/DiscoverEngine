/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/


#include "AudioManager.h"
#include "AudioSystem.h"
AudioManager::AudioManager()
{
	//creates a studio pointer fromn the audio system
	studio = NULL;
	AudioSystem::Errors(FMOD::Studio::System::create(&studio));
	AudioSystem::Errors(studio->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL));
	
	//setup fmod system to use
	system2 = NULL;
	AudioSystem::Errors(studio->getLowLevelSystem(&system2));
	//setup fmod settings and listerner position
	system2->set3DSettings(1.0f,1.0f,1.0f);
	FMOD_VECTOR  pos;
	pos.x = 0.0f;
	pos.y = 0.0f;
	pos.z = 0.0f;

	FMOD_VECTOR  vel;
	vel.x = 0.0f;
	vel.y = 0.0f;
	vel.z = 0.0f;

	FMOD_VECTOR  fw;
	fw.x = 0.0;
	fw.y = 0.0f;
	fw.z = 1.0;

	FMOD_VECTOR  up;
	up.x = 0.0;
	up.y = 1.0f;
	up.z = 0.0f;
	//creates the lister attributes and location
	system2->set3DListenerAttributes(0, &pos, &vel, &fw, &up);
}
AudioManager::~AudioManager()
{
	AudioSystem::Errors(studio->unloadAll());
	AudioSystem::Errors(studio->release());
}

void AudioManager::Update()
{

	//checks if sounds are playing
	std::vector<myChannels::iterator> stoppedChannels;
	for (auto it = channels.begin(), itEnd =channels.end();it!=itEnd;it++)
	{
		bool playing = false;
		it->second->isPlaying(&playing);
		if (!playing)
		{
			stoppedChannels.push_back(it);
		}
	}

	for (auto &it:stoppedChannels )
	{
		channels.erase(it);
	}
	AudioSystem::Errors(studio->update());
}