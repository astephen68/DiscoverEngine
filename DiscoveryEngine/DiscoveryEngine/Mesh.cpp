/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/




#include "Mesh.h"
#define INVALID_VALUE 0xffffffff

#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }
Mesh::MeshEntry::MeshEntry()
{
	VAO= INVALID_VALUE;
	VB = INVALID_VALUE;
	IB = INVALID_VALUE;
	NumIndices = 0;
	MaterialIndex = INVALID_MATERIAL;

}

Mesh::MeshEntry::~MeshEntry()
{
	if (VB!= INVALID_VALUE)
	{
		glDeleteBuffers(1, &VB);
	}
	if (IB!= INVALID_VALUE)
	{
		glDeleteBuffers(1, &IB);
	}
	if (VAO != INVALID_VALUE)
	{
		glDeleteVertexArrays(1, &VAO);
	}
}

void Mesh::MeshEntry::Init(const std::vector<Vertex> & Vertices,
	const std::vector<unsigned int>& Indices)
{
	NumIndices = Indices.size();

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1 ,&VB);
	glBindBuffer(GL_ARRAY_BUFFER,VB);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*Vertices.size(), &Vertices[0],GL_STATIC_DRAW);

	glGenBuffers(1, &IB);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IB);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*NumIndices, &Indices[0], GL_STATIC_DRAW);


}
Mesh::Mesh()
{

}


Mesh::~Mesh()
{
	Clear();
}


void Mesh::Clear()
{
	for (unsigned int i = 0; i < m_Textures.size(); i++)
	{
		SAFE_DELETE(m_Textures[i]);
	}
}
bool Mesh::LoadMesh(const std::string& Filename)
{
	//releases previously loaded mesh if exists
	Clear();
	
	bool Ret = false;
	Assimp::Importer Importer;
	pScene = Importer.ReadFile(Filename.c_str(),
		aiProcess_Triangulate | aiProcess_GenSmoothNormals |aiProcess_FlipUVs
	|aiProcess_JoinIdenticalVertices);

	if (pScene)
	{
		Ret = InitFromScene(pScene, Filename);
	}
	else
	{
		printf("Error parsing '%s: '%s' \n", Filename.c_str(), Importer.GetErrorString());

	}

	return Ret;
}

bool Mesh::InitFromScene(const aiScene* pScene, const std::string & Filename)
{
	m_Entries.resize(pScene->mNumMeshes);
	m_Textures.resize(pScene->mNumMaterials);

	for (unsigned int i = 0; i < m_Entries.size(); i++)
	{
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh);
	}

	return InitMaterials(pScene, Filename);
}
void Mesh::Update(glm::mat4 trans)
{
	for (int i = 0; i < Verticies.size(); i++)
	{
		glm::vec4 pos = glm::vec4(Verticies[i].Position,1.0f);
		glm::vec4 newpos = trans * pos;
		Verticies[i].Position = newpos;
	}
	for (int i = 0; i < m_Entries.size(); i++)
	{
		m_Entries[i].Init(Verticies, Indicies);
	}

		
}
void Mesh::SetPosition(glm::vec3 setPos)
{
	
		for (int i = 0; i < Verticies.size(); i++)
		{
			Verticies[i].Position= setPos+ orignVerticies[i].Position;
			Verticies[i].Normal = orignVerticies[i].Normal;
			Verticies[i].TexCoords = orignVerticies[i].TexCoords;


		}
		for (int i = 0; i < m_Entries.size(); i++)
		{
			m_Entries[i].Init(Verticies, Indicies);
		}

}
void Mesh::InitMesh(unsigned int Index, const aiMesh* paiMesh)
{
	m_Entries[Index].MaterialIndex = paiMesh->mMaterialIndex;
	//std::vector<Vertex> Verticies;
	//std::vector<unsigned int> Indicies;

	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
	for (unsigned int i = 0; i < paiMesh->mNumVertices; i++)
	{
		const aiVector3D* pPos = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal =(paiMesh->HasNormals()? &(paiMesh->mNormals[i]) : &Zero3D);
		const aiVector3D* pTexCoord = (paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]):&Zero3D);

		Vertex v;
		v.Position = glm::vec3(pPos->x, pPos->y, pPos->z);
		v.TexCoords = glm::vec2(pTexCoord->x, pTexCoord->y);
		v.Normal = glm::vec3(pNormal->x, pNormal->y, pNormal->z);
		Verticies.push_back(v);
	}
	for (unsigned int i = 0; i < paiMesh->mNumFaces; i++)
	{
		const aiFace& face = paiMesh->mFaces[i];
		assert(face.mNumIndices ==3);
		Indicies.push_back(face.mIndices[0]);
		Indicies.push_back(face.mIndices[1]);
		Indicies.push_back(face.mIndices[2]);

	}
	orignVerticies = Verticies;
	m_Entries[Index].Init(Verticies, Indicies);
}

bool Mesh::InitMaterials(const aiScene *pScene, const std::string &Filename)
{
	std::string::size_type SlashIndex = Filename.find_last_of("/");
	std::string Dir;

	if (SlashIndex==std::string::npos)
	{
		Dir = ".";
	}
	else if(SlashIndex==0)
	{
		Dir = "/";
	}
	else
	{
		Dir = Filename.substr(0, SlashIndex);
	}

	bool ret = true;
	for (unsigned int i = 0; i < pScene->mNumMaterials; i++)
	{
		const aiMaterial* pMaterial = pScene->mMaterials[i];
		m_Textures[i] = NULL;
		if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0)
		{
			aiString Path;

			if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL
				, NULL, NULL) == AI_SUCCESS)
			{
				std::string FullPath =Dir+ "/" + (std::string)Path.data;
				m_Textures[i] = new Texture(FullPath.c_str(), GL_REPEAT, GL_LINEAR, 0);


			}
		}
		if (!m_Textures[i]) {
			m_Textures[i] = new Texture("textures/brick.jpg", GL_REPEAT, GL_LINEAR, 0);
			m_Textures[i]->createTexture();
			//Ret = m_Textures[i]->Load();
		}
	}

	return true;
}

void Mesh::Render()
{



	for (unsigned int i = 0; i < m_Entries.size(); i++)
	{
		
		glBindBuffer(GL_ARRAY_BUFFER, m_Entries[i].VB);
		glBindVertexArray(m_Entries[i].VAO);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),(void*) offsetof(Vertex,TexCoords));
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex,Normal));
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Entries[i].IB);
		
		const unsigned int MaterialIndex = m_Entries[i].IB;

		if (MaterialIndex<m_Textures.size()&& m_Textures[MaterialIndex])
		{
			m_Textures[MaterialIndex]->bind(0);
		}
		glBindVertexArray(m_Entries[i].VAO);
		glDrawElements(GL_TRIANGLES, m_Entries[i].NumIndices, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
		
	}
	//glDisableVertexAttribArray(0);
	//glDisableVertexAttribArray(1);
	//glDisableVertexAttribArray(2);


}