/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include "fmod.hpp"
#include "fmod_studio.hpp"
#include <string>
#include <map>
#include <vector>
#include <math.h>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "AudioManager.h"

//used to manage sounds, events and banks in game
class AudioSystem
{
public:

	
	static void Init();
	static void Update();
	static void Exit();
	static int Errors(FMOD_RESULT result);

	void ChangeRollOff(FMOD_MODE mode);
	void LoadBankInformation(const std::string & name, FMOD_STUDIO_LOAD_BANK_FLAGS bank);
	void LoadEventInformation(const std::string & name);
	bool LoadSound(const std::string& name, bool b3d = true, bool loop = false, bool stream = false);
	void RemoveSound(const std::string &name);
	//sets postion and orienatation of sound
	int PlaySound(const std::string name, const glm::vec3& pos = glm::vec3(0.0f), float volume = 0.0f);
	void PlayEvent(const std::string &name);
	void StopChannel(int channel);
	void StopEvent(const std::string& name, bool immediate = false);
	void getEventParamter(const std::string name, const std::string & EventParameter, float* paramter);
	void setEventParameter(const std::string name, const std::string & paramterName, float value);
	void StopAllChannels();
	void SetPosition(int channel, const glm::vec3& pos);
	void SetChannelVolume(int channel, float volume);
	bool soundPlaying(int channel) const;
	bool eventPlaying(const std::string& name);
	float decibelsToVolume(float db);
	float volumeToDecibels(float volume);
	//converts glm to fmod vertex class to work with
	FMOD_VECTOR glmToFmod(const glm::vec3& pos);

	//sets the maximum and minimum hearing 
	void setMinMaxDistance(float min, float max, int channel);
	void setMode(int channelId, FMOD_MODE mode);
	
};