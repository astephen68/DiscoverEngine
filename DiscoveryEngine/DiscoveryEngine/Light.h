/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include "Shader.h"

class Light
{
private:
	Shader* myShader;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	glm::vec3 rim;
	float shiny;

	float constant;
	float linear;
	float quadratic;
public:
	Light(Shader* shader,glm::vec3 ambient,glm::vec3 diffuse,
		glm::vec3 specular, glm::vec3 r,float shiny,float constant,
		float linear,float quadratic);
	void setShader(Shader* shader);
	Shader* getShader();
	void setAmbient(glm::vec3 amb);
	glm::vec3 getAmbient();
	void setDiffuse(glm::vec3 diffuse);
	glm::vec3 getDiffuse();
	void setSpecular(glm::vec3 specular);
	glm::vec3 getSpecular();

	void setShiny(float shiny);
	float getShiny();
	void setConstant(float constant);
	float getConstant();
	void setLinear(float linear);
	float getLinear();
	void setQuadratic(float quad);
	float getQuadratic();
	void sendLight(glm::vec3 position, glm::vec3 dir, int amountLights);
	~Light();
};