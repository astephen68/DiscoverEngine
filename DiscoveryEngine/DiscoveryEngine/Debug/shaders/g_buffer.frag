#version 430 core

layout(location=0) out vec3 gPosition;
layout(location=1) out vec3 gNormal;
layout(location=2) out vec4 gAlbedoSpec;
layout(location=3) out vec4 spec;


in vec3 FragPos;
in vec2 TexCoords;
in vec3 Normal;

uniform sampler2D diffuse1;
uniform sampler2D specular1;


void main()
{
    gPosition=FragPos;
	gNormal=normalize(Normal);
	gAlbedoSpec.rgb=texture(diffuse1,TexCoords).rgb;
	gAlbedoSpec.a=1.0f;
	spec.rgb=texture(specular1,TexCoords).rgb;
	spec.a=1.0f;

	//albedo=vec4(1.0f,0.0f,0.0f,1.0f);

}  