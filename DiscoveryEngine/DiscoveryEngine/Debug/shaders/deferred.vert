
#version 430 core
layout(location=0) in vec3 aPos;
layout(location=1) in vec2 aTexCoords;


out vec2 TexCoords;


void main()
{
	//calculate frag pos in world space
	TexCoords=aTexCoords;
	gl_Position=vec4(aPos,0.0f);
}