#version 430 core
#define NR_POINT_LIGHTS 4

//point light
struct Light{
	vec3 lightPos;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shiny;
	vec3 rim;
	//attenuation for distance lighting
	float constant;
	float linear;
	float quadratic;
};


//send in info from vertex shader

in vec2 TexCoords;
out vec4 FragColor;
uniform Light lights[NR_POINT_LIGHTS];

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D gSpec;
uniform vec3 viewPos;

void main()
{             
    // retrieve data from gbuffer
    vec3 FragPos = texture(gPosition, TexCoords).rgb;
    vec3 Normal = texture(gNormal, TexCoords).rgb;
    vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
    vec3 Specular = texture(gSpec, TexCoords).rgb;
    
    // then calculate lighting as usual
    vec3 lighting  = Diffuse * 0.8; // hard-coded ambient component
    vec3 viewDir  = normalize(viewPos - FragPos);
    for(int i = 0; i < NR_POINT_LIGHTS; ++i)
    {
        // diffuse
        vec3 lightDir = normalize(lights[i].lightPos - FragPos);
        vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * lights[i].diffuse;
        // specular
        vec3 halfwayDir = normalize(lightDir + viewDir);  
        float spec = pow(max(dot(Normal, halfwayDir), 0.0), 16.0);
        vec3 specular =  spec * Specular;
        // attenuation
        float distance = length(lights[i].lightPos - FragPos);
        float attenuation = 1.0 / (1.0 + lights[i].linear * distance + lights[i].quadratic * distance * distance);
        diffuse *= attenuation;
        specular *= attenuation;
        lighting += diffuse + specular;        
    }
    FragColor = vec4(lighting, 1.0);
}