#version 430 core
#define NR_POINT_LIGHTS 1

//point light
struct Light{
	vec3 lightPos;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shiny;
	vec3 rim;
	//attenuation for distance lighting
	float constant;
	float linear;
	float quadratic;
};


//send in info from vertex shader

in vec2 TexCoords;
out vec4 FragColor;
uniform Light lights[NR_POINT_LIGHTS];

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform vec3 viewPos;

vec3 CalcPointLight(Light light, vec3 normal, vec3 fragPos, vec3 viewdir, vec3 diffuse, float Specular)
{
    vec3 lightDir = normalize(light.lightPos - fragPos);

	//diffuse shading
    vec3 diff =max(dot(normal,lightDir), 0.0)*diffuse*light.diffuse;


	//specualar shading
    vec3  halfwayDir = normalize(lightDir+viewdir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), light.shiny);
	vec3 specular=light.diffuse*spec*Specular;

	//attenuation to lighting(distance)
	float dist= length(light.lightPos - fragPos);
	float attenuation = 1.0 / (1.0+light.linear * dist +
			light.quadratic*dist*dist); 
  
	diff*=attenuation;
	specular*=attenuation;
	
	//tc+sc+
	return(diff+specular);
}





void main()
{

    vec3 FragPos = texture(gPosition, TexCoords).rgb;
    vec3 Normal = texture(gNormal, TexCoords).rgb;
    vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
    float Specular = texture(gAlbedoSpec, TexCoords).a;
	
	//normal information in model space
	vec3 viewDir = normalize(viewPos - FragPos);
	//total light calculations
	vec3 result=Diffuse*1.0;

	//calculate the lighting based on all lights
	for(int i=0;i<NR_POINT_LIGHTS; i++)
	{

		result+=CalcPointLight(lights[i],Normal,FragPos,viewDir,Diffuse,Specular);
	}


	FragColor=  vec4(result,1.0f);
  
}