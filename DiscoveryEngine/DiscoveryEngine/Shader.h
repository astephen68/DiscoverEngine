/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include <stdexcept>

using namespace std;
class Shader
{
public:
	/** Default constructor */
	Shader(const char *vertexFile, const char *fragmentFile);
	/** Default destructor */
	~Shader();
	GLchar *readFile(const char *fileName);
	bool createShader();
	void setVertexShader(const GLchar *vertexShader);
	const char* getVertexShader();
	void setFragmentShader(const GLchar *fragmentShader);
	const char* getFragmentShader();
	void unload();
	void useProgram();
	GLuint use();
	//send uniform information
	void sendUniformfMat4by4(const char* name, glm::mat4 trans);
	void sendUniformfvec3(const char* name, glm::vec3 trans);
	void sendFloat(const char* name, float trans);
	void sendInt(const char* name, int trans);

private:
	const char *vertexShader; //!< Member variable "*vertexShader"
	const char *fragmentShader; //!< Member variable "*fragmentShader"
	bool generateVertexShader();
	bool generateFragmentShader();
	void bind();
	bool generateProgram();

	GLuint vShader=0, fShader=0, shaderProgram=0;
	GLuint transLocation;
};