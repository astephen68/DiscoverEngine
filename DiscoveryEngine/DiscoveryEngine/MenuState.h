
/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#pragma once
#include "GameState..h"
#include "Mesh.h"

class MenuState :public GameState
{
private:
	 Mesh *startScreen;
	Texture *myTexture;

public:
	MenuState();
	~MenuState();
	void Init();
	void Cleanup();
	void Pause();
	void Resume();
	void HandleEvents();
	void Update();
	void Draw();




};
