
/*Name: Stephen Richards
ID: 100458273
Date: November 6,2017
*/
#version 330 core
layout(location=0) in vec3 aPos;
layout(location=1) in vec2 aTexCoords;
layout(location=2) in vec3 aNormal;
layout(location=3) in ivec4 BoneIDs;
layout (location = 4) in vec4 Weights;

const int MAX_BONES=200;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 gBones[MAX_BONES];

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;

void main()
{
    mat4 BoneTransform = gBones[BoneIDs[0]] * Weights[0];
    BoneTransform += gBones[BoneIDs[1]] * Weights[1];
    BoneTransform += gBones[BoneIDs[2]] * Weights[2];
    BoneTransform += gBones[BoneIDs[3]] * Weights[3];
	//BoneTransform=mat4(1.0f);
	//calculate frag pos in world space

	FragPos=vec3(model*BoneTransform*vec4(aPos,1.0f));
	Normal=mat3(transpose(inverse(model))) * aNormal;
	TexCoords=aTexCoords;
	gl_Position=projection*view*vec4(FragPos,1.0f);
}