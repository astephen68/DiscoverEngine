#version 430 core
#define NR_POINT_LIGHTS 1

struct Material {
	sampler2D diffuse;
	sampler2D specular;
	float shininess;
};

//point light
struct Light{
	vec3 lightPos;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shiny;
	vec3 rim;
	//attenuation for distance lighting
	float constant;
	float linear;
	float quadratic;
};


//send in info from vertex shader
in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;


//uniforms
//material uniform for assimp loader if theres materials
uniform Material material;
//eye position of the camera passed in camera class
uniform vec3 viewPos;
//lights point lights position array
uniform Light lights[NR_POINT_LIGHTS];



uniform sampler1D test;
uniform sampler2D test2;
uniform sampler3D lut;
uniform float lutsize;


//toggles for lighting
uniform int rimOn;
uniform int filterOn;
uniform int diffRam;
uniform int specRam;

out vec4 FragColor;

vec3 CalcPointLight(Light light, vec3 normal, vec3 fragPos, vec3 viewdir)
{
	vec3  tc=vec3(0.0f);
	vec3 sc=vec3(0.0f);

    vec3 lightDir = normalize(light.lightPos - fragPos);

	//diffuse shading
    float diff =max(dot(lightDir,normal), 0.0);
	if(diffRam==1){
	   tc=texture(test,diff).xyz;
		}


	//specualar shading
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = pow(max(dot(viewdir, reflectDir), 0.0), light.shiny);
	if(specRam==1){
	sc=texture(test,spec).xyz;}


	//attenuation to lighting(distance)
	float dist= length(light.lightPos - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * dist + 
    				light.quadratic * (dist * dist));
  

	vec3 ambient=light.ambient*vec3(texture(test2, TexCoords));
    vec3 diffuse =light.diffuse*diff*vec3(texture(test2, TexCoords));
    vec3 specular =light.specular* spec*vec3(texture(test2, TexCoords));
	 

	ambient*=attenuation;
	diffuse*=attenuation;
	specular*=attenuation;
	//tc+sc+
	return(tc+sc+ambient+diffuse+specular);
}


vec3 rimLight(Light light, vec3 n, vec3 v)
{
	float rim= 1.0-max(dot(v,n),0.0);
	rim=smoothstep(0.6,1.0,rim);
	vec3 finalRim=light.rim*rim;
	return finalRim;
}

//lookup table filter for color corrections
vec3 lutFilterInformation(sampler3D lutTable,vec3 lightCalculations, float size)
{
	//scale of the lut information 
	vec3 scale=vec3((size-1.0f)/size);
	//offset of the information of the lookuptable due to opengl and after effects coodinate system are different
	vec3 offset=vec3(1.0/(2.0*size));

	//returns the 3d texture of the lookup table information
	return texture(lutTable,scale*lightCalculations+offset).rgb;

}


void main()
{
	//normal information in model space
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);
	//total light calculations
	vec3 result;

	//calculate the lighting based on all lights
	for(int i=0;i<NR_POINT_LIGHTS; i++)
	{

		result+=CalcPointLight(lights[i],norm,FragPos,viewDir);
	}

    if(rimOn==1)
	{	
		result+=rimLight(lights[0],norm,viewDir);
	}
	if(filterOn==1)
	{

		result=lutFilterInformation(lut,result,lutsize);
	}

	//return colored information

	FragColor=  vec4(result,1.0f);
  
}