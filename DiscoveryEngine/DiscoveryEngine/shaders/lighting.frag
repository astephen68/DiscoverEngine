/*Name: Stephen Richards
ID: 100458273
Date: November 6,2017
*/
#version 330 core

struct Material {
	sampler2D diffuse;
	sampler2D specular;
	float shininess;
};

//point light
struct Light{
	vec3 lightPos;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shiny;
	//attenuation for distance lighting
	float constant;
	float linear;
	float quadratic;
};

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;

//uniform Light light;
uniform Material material;
uniform vec3 viewPos;
#define NR_POINT_LIGHTS 3

uniform Light lights[NR_POINT_LIGHTS];

out vec4 FragColor;

vec3 CalcPointLight(Light light, vec3 normal, vec3 fragPos, vec3 viewdir)
{

    vec3 lightDir = normalize(light.lightPos - fragPos);
	//diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
	//specualar shading
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = pow(max(dot(viewdir, reflectDir), 0.0), light.shiny);
	//attenuation to lighting(distance)
	float dist= length(light.lightPos - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * dist + 
    				light.quadratic * (dist * dist));    

	//material ambient when not using shape color
 
	vec3 ambient=light.ambient*vec3(texture(material.diffuse,TexCoords));
    vec3 diffuse =light.diffuse*diff*vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular*spec*vec3(texture(material.specular,TexCoords)); 

	ambient*=attenuation;
	diffuse*=attenuation;
	specular*=attenuation;
	return (ambient + diffuse+ specular);
}

void main()
{
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 result;
	for(int i=0;i<NR_POINT_LIGHTS; i++)
	{
		result+=CalcPointLight(lights[i],norm,FragPos,viewDir);
		
	}
	//FragColor =  vec4(result, 1.0);

	FragColor=vec4(result,1.0f);
  
}