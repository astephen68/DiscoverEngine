/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/



#include "Model.h"

Model::Model(Mesh* mesh)
{
	myMesh = mesh;
	scale = 1.0f;
	pos = glm::vec3(0.0f, 0.0f, 0.0f);
}
Model::~Model()
{
	myMesh = nullptr;

}
//Skeleton sceneLoaderSkeleton;
void Model::setPosition(glm::vec3 p)
{
	myMesh->SetPosition(p);
	pos = p;
}

glm::vec3 Model::getPosition()
{
	return pos;
}
float Model::getScale()
{
	return scale;
}
void Model::setScale(float sca)
{
	scale = sca;
}
void Model::setRotation(float x, float y, float z)
{
	rotx = x;
	roty = y;
	rotz = z;
}
void Model::setTranslation(float x, float y, float z)
{
	transx = x;
	transy = y;
	transz = z;
}
void Model::Draw()
{
	myMesh->Render();
}
void Model::Update(Shader* myShader)
{
	glm::mat4 trans(1.0f), rx(1.0f), ry(1.0f), rz(1.0f), rot(1.0f), sca(1.0f);
	glm::mat4 global = glm::mat4(1.0f);
	pos = glm::vec3(transx, transy, transz);
	trans = glm::translate(trans, glm::vec3(transx, transy, transz));
	rx = glm::rotate(rx, glm::radians(rotx), glm::vec3(1.0f, 0.0f, 0.0f));
	ry = glm::rotate(ry, glm::radians(roty), glm::vec3(0.0f, 1.0f, 0.0f));
	rz = glm::rotate(rz, glm::radians(rotz), glm::vec3(0.0f, 0.0f, 1.0f));
	rot = rx * ry*rz;
	sca = glm::scale(sca, glm::vec3(scale, scale, scale));
	transform = trans * rot*sca;
	myShader->sendUniformfMat4by4("model", transform);
	transx = 0, transy = 0, transz = 0;

}