/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <string.h>
#include <vector>
#include "Bone.h"
#include<sstream>
#include "Texture.h"
#include "Shader.h"



struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	//glm::vec4 Colors;
	//float weight[4];
	//unsigned int id[4];
};

struct Textures {
	unsigned int id;
	std::string type;
	aiString path;
};


class Mesh{
private:
	virtual bool InitFromScene(const aiScene* pScene, const std::string& Filename);
	void InitMesh(unsigned int Index, const aiMesh* paiMesh);
	virtual bool InitMaterials(const aiScene* pScene, const std::string& Filename);
	virtual void Clear();

	std::vector<Vertex> orignVerticies;
	std::vector<Vertex> Verticies;
	std::vector<unsigned int> Indicies;

#define INVALID_MATERIAL 0xFFFFFFFF
	struct MeshEntry {
		MeshEntry();

		~MeshEntry();

		void Init(const std::vector<Vertex>& Vertices, const std::vector<unsigned int> &Indices);

		GLuint VAO;
		GLuint VB;
		GLuint IB;
		unsigned int NumIndices;
		unsigned int MaterialIndex;


	};
	const aiScene* pScene;
	std::vector<MeshEntry>m_Entries;
	std::vector<Texture*> m_Textures;

public:
	Mesh();
	~Mesh();

	 virtual bool LoadMesh(const std::string& Filename);
	 virtual void Render();
	 void Update(glm::mat4 trans);
	 void SetPosition(glm::vec3 setPos);

};