/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Window
{

private:
	GLFWwindow * myWindow;
	GLuint myWidth, myHeight;
	const char* myTitle;
	//NULL
	GLFWmonitor* myMonitor;
	GLFWwindow* myShare;
public:
	Window();
	Window(GLuint width, GLuint height, const char* title, GLFWmonitor* monitor, GLFWwindow* share);
	~Window();
	GLFWwindow* CreateGlfwWindow();
	void Init();
	void Unload();
	void Update();

};