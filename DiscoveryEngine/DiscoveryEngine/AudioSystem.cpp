/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/



#include "AudioSystem.h"


AudioManager *manager = nullptr;

void AudioSystem::Init()
{
	
	manager = new AudioManager();
}

void AudioSystem::Update()
{
	manager->Update();
}

bool AudioSystem::LoadSound(const std::string& filename, bool b3d, bool loop, bool stream)
{
	auto sound = manager->sounds.find(filename);
	if (sound != manager->sounds.end())
	{
		return false;
	}
	FMOD_MODE eMode = FMOD_DEFAULT;
	eMode |= b3d ? FMOD_3D : FMOD_2D;
	eMode |= loop ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
	eMode |= stream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;

	FMOD::Sound* soundP = nullptr;
	AudioSystem::Errors(manager->system2->createSound(filename.c_str(),eMode,nullptr,&soundP));
	if (soundP)
	{
		manager->sounds[filename] = soundP;
		return true;
	}
	return false;
}

void AudioSystem::RemoveSound(const std::string &sound)
{
	auto found = manager->sounds.find(sound);
	if (found==manager->sounds.end())
	{
		return;
	}
	AudioSystem::Errors(found->second->release());
	manager->sounds.erase(found);
}

int AudioSystem::PlaySound(const std::string name, const glm::vec3& pos, float volume)
{
	int channel = manager->nextChannel++;
	auto found = manager->sounds.find(name);
	if (found==manager->sounds.end())
	{
		LoadSound(name);

		found = manager->sounds.find(name);
		if (found==manager->sounds.end())
		{
			return channel;
		}
	}

	FMOD::Channel* myChan = nullptr;
	AudioSystem::Errors(manager->system2->playSound(found->second, nullptr, true, &myChan));
	if (myChan)
	{
		FMOD_MODE mode;
		found->second->getMode(&mode);
		if (mode&FMOD_3D)
		{
			FMOD_VECTOR position = glmToFmod(pos);
			AudioSystem::Errors(myChan->set3DAttributes(&position, nullptr));

		}
		AudioSystem::Errors(myChan->setVolume(decibelsToVolume(volume)));
		AudioSystem::Errors(myChan->setPaused(false));
		manager->channels[channel] = myChan;
	}

	return channel;
}

void AudioSystem::SetPosition(int id, const glm::vec3& pos)
{
	auto found = manager->channels.find(id);
	if (found==manager->channels.end())
	{
		return;
	}

	FMOD_VECTOR position = glmToFmod(pos);
	AudioSystem::Errors(found->second->set3DAttributes(&position, NULL));

}

void AudioSystem::SetChannelVolume(int channel, float volume)
{
	auto found = manager->channels.find(channel);
	if (found==manager->channels.end())
	{
		return;
	}
	AudioSystem::Errors(found->second->setVolume(decibelsToVolume(volume)));
}

bool AudioSystem::soundPlaying(int channel) const
{
	//auto found = manager->channels.find(channel);
	//if (found == manager->channels.end())
	//{
	//	return false;
	//}
	//bool result = false;
	//found->second->isPlaying(&result);
	//if(result)
	//{
	//	return true;
	//}
	//return false;
	return false;
}

void AudioSystem::ChangeRollOff(FMOD_MODE mode)
{
	manager->system2->set3DSettings(1.0f, 1.0f, mode);
}

void AudioSystem::LoadBankInformation(const std::string& name, FMOD_STUDIO_LOAD_BANK_FLAGS flags)
{
	auto found = manager->banks.find(name);
	if (found!= manager->banks.end())
	{
		return;
	}
	FMOD::Studio::Bank *bank;
	AudioSystem::Errors(manager->studio->loadBankFile(name.c_str(), flags, &bank));
	if (bank)
	{
		manager->banks[name] = bank;
	}
}

void AudioSystem::LoadEventInformation(const std::string & name)
{
	auto found = manager->events.find(name);
	if (found!=manager->events.end())
	{
		return;
	}
	FMOD::Studio::EventDescription* eventDes = NULL;
	AudioSystem::Errors(manager->studio->getEvent(name.c_str(), &eventDes));

	if (eventDes)
	{
		FMOD::Studio::EventInstance * eventInst = NULL;
		AudioSystem::Errors(eventDes->createInstance(&eventInst));
		if (eventInst)
		{
			manager->events[name] = eventInst;
		}

	}
}

void AudioSystem::PlayEvent(const std::string &name)
{
	auto found = manager->events.find(name);
	if (found==manager->events.end())
	{
		LoadEventInformation(name);
		found = manager->events.find(name);
		if (found==manager->events.end())
		{
			return;
		}
	}
	found->second->start();
}

void AudioSystem::StopChannel(int channel)
{
	auto found = manager->channels.find(channel);
	if (found == manager->channels.end())
		return;

	found->second->stop();
}

void AudioSystem::StopEvent(const std::string &name, bool imm)
{
	auto found = manager->events.find(name);
	if (found==manager->events.end())
	{
		return;
	}
	FMOD_STUDIO_STOP_MODE eMode;
	eMode = imm ? FMOD_STUDIO_STOP_IMMEDIATE : FMOD_STUDIO_STOP_ALLOWFADEOUT;
	AudioSystem::Errors(found->second->stop(eMode));
}

bool AudioSystem::eventPlaying(const std::string &name) 
{
	auto found = manager->events.find(name);
	if (found==manager->events.end())
	{
		return false;
	}
	FMOD_STUDIO_PLAYBACK_STATE* state = NULL;
	if (found->second->getPlaybackState(state)==FMOD_STUDIO_PLAYBACK_PLAYING)
	{
		return true;
	}
	return false;

}

void AudioSystem::getEventParamter(const std::string event, const std::string &parm,  float * par)
{
	auto found = manager->events.find(event);
	if (found==manager->events.end())
	{
		return;
	}

	FMOD::Studio::ParameterInstance* pparm = NULL;
	AudioSystem::Errors(found->second->getParameter(parm.c_str(), &pparm));
	AudioSystem::Errors(pparm->getValue(par));
}

void AudioSystem::setEventParameter(const std::string event,const std::string &paramter,float value)
{
	auto found = manager->events.find(event);
	if (found == manager->events.end())
		return;

	FMOD::Studio::ParameterInstance* pparm= NULL;
	AudioSystem::Errors(found->second->getParameter(paramter.c_str(), &pparm));
	AudioSystem::Errors(pparm->setValue(value));

}

void AudioSystem::StopAllChannels()
{
}

FMOD_VECTOR AudioSystem::glmToFmod(const glm::vec3& pos)
{
	FMOD_VECTOR vector;
	vector.x = pos.x;
	vector.y = pos.y;
	vector.z = pos.z;
	return vector;
}

void AudioSystem::setMinMaxDistance(float min, float max, int channel)
{
	auto found = manager->channels.find(channel);
	if (found == manager->channels.end())
	{
		return;
	}
	found->second->set3DMinMaxDistance(min, max);

}

void AudioSystem::setMode(int channelId, FMOD_MODE mode)
{
	auto found = manager->channels.find(channelId);
	if (found == manager->channels.end())
	{
		return;
	}
	found->second->setMode(mode);
}

float AudioSystem::decibelsToVolume(float decibel)
{
	return powf(10.0f, 0.05f*decibel);
}

float AudioSystem::volumeToDecibels(float volume)

{
	return 20.0f*log10f(volume);
}
int AudioSystem::Errors(FMOD_RESULT result)
{
	if (result!=FMOD_OK)
	{
		std::cout << "FMOD HAS STOPPED WORKING " << result << std::endl;
		return 1;
	}
	return 0;
}

void AudioSystem::Exit()
{
	delete manager;
}

