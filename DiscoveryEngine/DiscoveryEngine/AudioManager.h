/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#pragma once
#include "fmod.hpp"
#include "fmod_studio.hpp"
#include <string>
#include <iostream>
#include <map>




class AudioManager
{

public:
	AudioManager();
	~AudioManager();


	void Update();

	//uses fmod studio and low level fmod
	FMOD::Studio::System* studio;
	FMOD::System* system2;
	
	int nextChannel;

	//manages the different sounds, channels and events  for my audio engine
	//stores all mny channels eents, banks and sounds that i use in game
	typedef std::map<std::string, FMOD::Sound*> mySounds;
	typedef std::map<int, FMOD::Channel*> myChannels;
	typedef std::map<std::string, FMOD::Studio::EventInstance*> myEvents;
	typedef std::map<std::string, FMOD::Studio::Bank*> myBanks;


	mySounds sounds;
	myChannels channels;
	myEvents events;
	myBanks banks;



};