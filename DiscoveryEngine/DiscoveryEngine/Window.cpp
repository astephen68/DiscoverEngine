/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#include "Window.h"


Window::Window()
{
	myWidth = 800;
	myHeight = 600;
	myTitle = "Default";
	myMonitor = NULL;
	myShare = NULL;
}
Window::Window(GLuint width, GLuint height, const char* title,
	GLFWmonitor* monitor, GLFWwindow* share)
{
	myWidth = width;
	myHeight = height;
	myTitle = title;
	myMonitor = monitor;
	myShare = share;
}
Window::~Window()
{
	Unload();
}

GLFWwindow* Window::CreateGlfwWindow()
{
	Init();
	//width,height,title,windowed or full screen, share resources / creates a window
	 myWindow= glfwCreateWindow(myWidth,myHeight,myTitle,myMonitor,myShare);
	if (myWindow == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return nullptr;
	}
	//sets focus to new window
	glfwMakeContextCurrent(myWindow);
	return myWindow;
}
void Window::Init()
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}
void Window::Unload()
{
	glfwDestroyWindow(myWindow);
	myWindow = NULL;
}
void Window::Update()
{

	//swaps the color buffer
	glfwSwapBuffers(myWindow);
	//checks for user input
	glfwPollEvents();
}

