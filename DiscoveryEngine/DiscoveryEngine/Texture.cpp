/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#define LUTSIZE 32*32*32*3


#include <fstream>
#include <string>
#include "Texture.h"
#include "stb_image.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
Texture::Texture()
{
	this->textureUnit = 0;
	this->fileName = "textures/wall2.jpg";
	this->wrap = GL_REPEAT;
	this->filter = GL_LINEAR;
}
Texture ::Texture(const char* fileName, GLenum wrap=GL_REPEAT, GLenum filter=GL_LINEAR,int textureUnit=0)
{
	this->textureUnit = textureUnit;
	this->fileName = fileName;
	this->wrap = wrap;
	this->filter = filter;
}
Texture::~Texture()
{
	unload();
}
void Texture::addTexture(const char * fileName)
{
	textures.push_back(fileName);
}
unsigned int Texture:: getId()
{
	return texture;
}
void Texture::bind(int textureUnit)
{
	if (textureUnit<0)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
		return;
	}
	//sets the texture unit
	glActiveTexture(GL_TEXTURE0+textureUnit);
	if (textureUnit==0)
	{
		glBindTexture(GL_TEXTURE_2D, texture);

	}
	else if (textureUnit==1)
	{
		glBindTexture(GL_TEXTURE_1D, texture);
	}
	else if (textureUnit==2)
	{
		glBindTexture(GL_TEXTURE_3D, texture);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, texture);
	}
	
}
unsigned int Texture::createCubeMap()
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	for (int i = 0; i < textures.size(); i++)
	{
		myTexture = stbi_load(textures[i].c_str(), &width, &height, &nrChannels, STBI_rgb_alpha);
		if (myTexture)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, myTexture);
			stbi_image_free(myTexture);
		}
		else
		{
			return -1;
		}
	}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER,filter);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,wrap);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrap);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrap);

	

		return texture;
}
bool Texture::createTexture()
{

	//1. specificies the target texture, so 2d
	//2. specifies the wraping mode or filter mode
	//3. specifies the specific wrap
	return LoadTexture();
}

bool Texture::create3DTexture(const char * fileName)
{

	//32 height, 32 width, 32 depth, 3 floats per value
	lut= new float[LUTSIZE];

	//fill the table with 0's to be populated when we get the lookup table 
	for (int i = 0; i <LUTSIZE; i++)
	{
		lut[i] = 0.0f;
	}

	//read file information using ifstream
	std::ifstream file;
	std::string line;
	file.open(fileName);

	//checks if file can be open if so read the file
	if(file.is_open())
	{
		//set postiion in array
		int count = 0;
		while (std::getline(file, line))
		{
			float x, y, z;
			//checks to see if line includes and blank space, or title, lut size as we dont want to read
			// these lines due to .cube format
			if (line.find("TITLE")==std::string::npos && line.find("LUT_3D_SIZE") == std::string::npos
				&&line!="")
			{
				int post = line.find(" ");
			     x = std::stof(line.substr(0, post));
				lut[count] = x;
				count++;
				int posty = post;
				post = line.find(" ", post + 1);
				  y= std::stof(line.substr(posty, post));
				lut[count] = y;
				count++;
				 z = std::stof(line.substr(post + 1));
				lut[count] = z;
				count++;
			}
		}

	}
	lutsize = 32;
	int size =32;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_3D, texture);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, wrap);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, wrap);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, filter);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB8,size,size,size,0,GL_RGB, GL_FLOAT,&lut[0]);
	glBindTexture(GL_TEXTURE_3D, 0);

	delete[] lut;
	return true;
}

bool Texture::create1DTexture()
{
	myTexture = stbi_load(fileName, &width, &height, &nrChannels, STBI_rgb_alpha);

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_1D, texture);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, wrap);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, filter);
	glTexImage1D(GL_TEXTURE_1D, 0,GL_RGBA, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, myTexture);
	glBindTexture(GL_TEXTURE_1D, 0);

	stbi_image_free(myTexture);
	return true;
}

bool Texture::LoadTexture()
{
	//loads the texture via stbi library
	//stbi_set_flip_vertically_on_load(true);
	myTexture = stbi_load(fileName, &width, &height, &nrChannels,STBI_rgb_alpha);




	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, myTexture);
	//glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	glBindTexture(GL_TEXTURE_2D, 0);

	stbi_image_free(myTexture);
	return true;

}

void Texture::unbind()
{
	glDisable(GL_TEXTURE_2D);
}
void Texture::update()
{

}
void Texture::unload()
{
	//stbi_image_free(myTexture);
	myTexture = nullptr;
	texture = 0;
}