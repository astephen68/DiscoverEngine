#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

class FrameBuffer
{
private:
	unsigned int fbo;
	unsigned int depthID;
	unsigned int textureID;
	unsigned int rbo;
	int height;
	int width;
	void createTextureAttachment();
	void createDepthAttachment();

	void createRenderBuffer();
public:
	FrameBuffer(int height, int width);
	~FrameBuffer();
	bool BindForColor();
	void bind();
	void bindTexture();

};