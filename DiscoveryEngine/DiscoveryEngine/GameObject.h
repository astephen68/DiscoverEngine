/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once

#include <GLM/glm.hpp>
#include <GLM\gtx\transform.hpp>
#include <GLM\gtc\type_ptr.hpp>
#include <string>

#include "SkinnedMesh.h"
#include "Model.h"
#include <memory>



class GameObject
{
private:
	

	
public:
	SkinnedMesh * model;
	GameObject *blendedModel;
	std::vector<glm::mat4> Transforms;
	bool anim;
	bool blend = false;
	std::vector <Animation> animations;
	Skeleton skeleton;

	GameObject(SkinnedMesh * myModel);
	virtual void UpdateSkeleton(float currentFrame);
	virtual void Draw();
	virtual void AddAnimation(Animation& in_anim);
	virtual Animation* FindAnimation(std::string anim_to_find);
	virtual void PlayAnimation(Animation& anim, bool loop = false, bool reset_to_start = false);
	virtual void StopAnimating();




};