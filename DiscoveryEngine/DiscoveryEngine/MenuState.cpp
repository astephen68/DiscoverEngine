
/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#include "MenuState.h"


MenuState::MenuState()
{

}
MenuState::~MenuState()
{

}

void MenuState::Init()
{
	startScreen = new Mesh();
	startScreen->LoadMesh("models/start.obj");
	myTexture = new Texture("textures/startscreen.png", GL_REPEAT, GL_LINEAR, 0);
	myTexture->createTexture();
}
void MenuState::Cleanup()
{

}
void MenuState::Pause()
{

}
void MenuState::Resume()
{

}
void MenuState::HandleEvents()
{

}
void MenuState::Update()
{

}
void MenuState::Draw()
{
	myTexture->bind();
	startScreen->Render();

}