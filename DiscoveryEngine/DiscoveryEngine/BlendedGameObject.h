/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once

#include <GLM/glm.hpp>
#include <GLM\gtx\transform.hpp>
#include <GLM\gtc\type_ptr.hpp>
#include <string>

#include "SkinnedMesh.h"
#include "Model.h"
#include <memory>
#include "GameObject.h"

class BlendedGameObject
{
private:
	std::vector<GameObject*> ObjectsToBlend;
	SkinnedMesh *newMesh;
public:
	BlendedGameObject(GameObject * first, GameObject* second);
	~BlendedGameObject();

	void Update(float currentFrame);
	void AddGameObject(GameObject * gameObject);
	void Draw(float );

	bool anim;
	std::vector <Animation> animations;

	void AddAnimation(Animation& in_anim);
     Animation* FindAnimation(std::string anim_to_find);
	void PlayAnimation(Animation& anim, bool loop = false, bool reset_to_start = false);
	void StopAnimating();
};