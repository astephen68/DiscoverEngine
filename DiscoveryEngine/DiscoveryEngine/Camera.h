/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include "Shader.h"

//structure to load camera
struct CameraDetails {
	glm::vec3 pos;
	glm::vec3 up;
	glm::vec3 direction;
	glm::vec3 front;
	glm::vec3 cameraUp;
	glm::vec3 target;
	Shader* myShader;

	CameraDetails(glm::vec3 position, glm::vec3 front,glm::vec3 YCamera, glm::vec3 target, Shader* shader)
	{
		this->front = front;
		pos = position;
		up = YCamera;
		cameraUp = up;
		this->target = target;
		myShader = shader;
	}

};


class Camera {


private:

	//location of camera
	glm::vec3 position;

	glm::vec3 direction;
	//up position or y-axis for the camera default 0,1,0 for world
	glm::vec3 up;
	//x-axis cross product with nom
	glm::vec3 right;
	//front view for the camera z-axis
	 glm::vec3 front;
	 //camera target
	 glm::vec3 target;
	//y axis for the world
	 glm::vec3 cameraUp;
	//view to generate
	glm::mat4 view;

	//shader to use to send information to the pointed shader
	Shader* myShader;
public:
	Camera();
	Camera(CameraDetails cd);
	void setShader(Shader *shader);
	glm::vec3 getPosition();
	Camera(glm::vec3 postion, glm::vec3 worldup,glm::vec3 target, glm::vec3 front,  Shader* shader);
	void moveCamera(float cameraSpeed,  std::string movement);
	void rotateCamera(double xpos, double ypos);
	void Update();
	void Process();
	void setPosition(glm::vec3 pos);
	void setYaw(float yaw);
	void setPitch(float pit);

	glm::mat4 getView();
	void Unload();

	~Camera();


};
