/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#pragma once


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Animation.h"
#include <string>
#include <assimp/scene.h>
#include <iostream>
#include <vector>

class Mesh;
class Animation;
class GameObject;
class Skeleton;

static aiMatrix4x4 GLMMat4ToAi(glm::mat4 mat)
{
	return aiMatrix4x4(mat[0][0], mat[0][1], mat[0][2], mat[0][3],
		mat[1][0], mat[1][1], mat[1][2], mat[1][3],
		mat[2][0], mat[2][1], mat[2][2], mat[2][3],
		mat[3][0], mat[3][1], mat[3][2], mat[3][3]);
}

static glm::mat4 AIToGLMMat4(aiMatrix4x4& in_mat)
{
	glm::mat4 tmp;
	tmp[0][0] = in_mat.a1;
	tmp[1][0] = in_mat.a2;
	tmp[2][0] = in_mat.a3;
	tmp[3][0] = in_mat.a4;


	tmp[0][1] = in_mat.b1;
	tmp[1][1] = in_mat.b2;
	tmp[2][1] = in_mat.b3;
	tmp[3][1] = in_mat.b4;

	
	tmp[0][2] = in_mat.c1;
	tmp[1][2] = in_mat.c2;
	tmp[2][2] = in_mat.c3;
	tmp[3][2] = in_mat.c4;


	tmp[0][3] = in_mat.d1;
	tmp[1][3] = in_mat.d2;
	tmp[2][3] = in_mat.d3;
	tmp[3][3] = in_mat.d4;

	return tmp;

}


class Bone {

public:
	std::string name;

	Mesh* mesh;

	unsigned int id;
	aiNode* node;

	aiNodeAnim* animNode;

	Bone* parent_bone;

	glm::mat4 parent_transforms;

	glm::mat4 offset_matrix;

	Skeleton* parent_skeleton;


	//keyframing can use produced class, temp for testing
	glm::vec3 pos;
	glm::quat rot;
	glm::vec3 scale;
	glm::vec3 p1;
	glm::vec3 p2;

	unsigned int FindPosition(float time);
	glm::vec3 CalcInterpolatedPosition(float time);

	unsigned int FindRotation(float time);
	glm::quat CalcInterpolatedRotation(float time);

	Bone() { name = ""; id = -2; }

	Bone(Mesh* in_mesh, unsigned int in_id, std::string in_name, aiMatrix4x4 in_o_mat);
	Bone(Mesh* in_mesh, unsigned int in_id, std::string in_name, glm::mat4 in_o_mat);

	void UpdateKeyframeTransform(float time);
	glm::mat4 GetParentTransforms();


};

class Skeleton
{
	public:
		std::vector<Bone> bones;
		glm::mat4 globalInverseTransform;
		std::vector<glm::mat4> boneMats;

		float time;
		float start_time;
		float end_time;

		Animation* active_animation;
		Animation* idle_animation;

		bool anim_play;
		bool anim_loop;

		float frameTime;

		Skeleton() {
			time = start_time = end_time = 0;
			active_animation = nullptr;
			anim_loop = false;

		
		
		}
		Skeleton(std::vector<Bone> in_bones, glm::mat4 in_globalInverseTransform)
		{
			Init(in_bones, in_globalInverseTransform);
		}
		void Init(std::vector<Bone> in_bones, glm::mat4 in_globalInverseTransform)
		{
			bones = in_bones;
			globalInverseTransform = in_globalInverseTransform;
			time = start_time = end_time = 0;
			active_animation = nullptr;
			idle_animation = nullptr;
			anim_loop = false;
			for (int i = 0; i < bones.size(); i++)
			{
				bones.at(i).parent_skeleton = this;
			}
		}

		Bone* FindBone(std::string name)
		{
			for (int i = 0; i < bones.size(); i++)
			{
				if (bones.at(i).name==name)
				{
					return &bones.at(i);
				}
			}
			return nullptr;
		}

		void PlayAnimation(Animation& anim, bool loop, bool reset_to_start)
		{
			if (active_animation != nullptr)
			{

				if (anim.priority < active_animation->priority)
				{
					active_animation = &anim;
				}
				else
				{
					return;
				}
			}
			else
			{
				active_animation = &anim;
				start_time = active_animation->start_time;
				end_time = active_animation->end_time;
				anim_play = true;
				anim_loop = loop;
				if (reset_to_start)
				{

					time = active_animation->start_time;
				}
			}

		}

		void StopAnimating()
		{
			time = end_time;
			active_animation = nullptr;
			anim_play = false;
		}

		void SetIdleAnimation(Animation* in_anim)
		{
			idle_animation = in_anim;
		}

		void UpdateBoneMatsVector()
		{
			if (bones.size()==0)
			{
				return;
			}

			boneMats.clear();

			for (int i = 0; i < 118; i++)
			{
				if (i>bones.size()-1)
				{
					boneMats.push_back(glm::mat4(1.0f));
				}
				else
				{
					//BASICALLY WHERE THE MATH GOES WRONG, need to fix
					glm::mat4 concatenated_transformation = (bones.at(i).GetParentTransforms()
						*AIToGLMMat4(bones.at(i).node->mTransformation));
					glm::mat4 send = globalInverseTransform*concatenated_transformation*bones.at(i).offset_matrix;
					boneMats.push_back(send);

				}
			}
		}
		void Update()
		{
			UpdateBoneMatsVector();
			if (!anim_play)
			{
				return;
			}

			time += frameTime*0.001f;

			if (time<start_time)
			{
				time = start_time;
			}

			if (time>end_time)
			{
				time = start_time;

			}
			else
			{
				StopAnimating();
			}
			for (int i = 0; i < bones.size(); i++)
			{
				bones.at(i).UpdateKeyframeTransform(time);
			}
		}
};