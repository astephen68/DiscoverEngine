/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

#include "Camera.h"
#include "AnimationMath.h"

//camera settings
bool firstMouse = true;
float yaw = -90.0f;	
//anle to look camera up or down
float pitch = -75.0f;
float lastX = 800.0 / 4.0;
float lastY = 600.0 / 4.0;
float fov = 45.0f;

glm::mat4 Camera::getView()
{
	return view;
}

Camera::Camera()
{
	//positioning of the camera in the world
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	//direction
	front=glm::vec3(0.0f, 0.0f, -1.0f);
	//y-axis in world space
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	//y-axis of cameria
	cameraUp = up;
	target = glm::vec3(0.0f, 0.0f, 0.0f);
	myShader = new Shader("shaders/main.vert","shaders/main.frag");
	myShader->createShader();
	Process();
}

Camera::Camera(CameraDetails cd)
{
	position = cd.pos;
	front = cd.front;
	up = cd.up;
	cameraUp = cd.cameraUp;
	target = cd.target;
	myShader = cd.myShader;
}


Camera::Camera(glm::vec3 postion, glm::vec3 cameraup, glm::vec3 target,  glm::vec3 front, Shader* shader)
{
	this->position = postion;;
	this->up = cameraup;
	this->front = front;

	myShader = shader;
	Process();

}


void Camera::setPosition(glm::vec3 pos)
{
	position = pos;
}
void Camera:: setYaw(float y)
{
	yaw = y;
}
void Camera::setPitch(float pit)
{
	pitch = pit;
}
//move camera via arrow keys
void Camera::moveCamera(float cameraSpeed, std::string movement)
{
	glm::vec3 temp,temp2;
	if (movement=="forward")
	{
		temp2 = position + front*cameraSpeed;
		temp = AnimationMath::lerp(position, temp2, 0.5f);
		position = temp;
	}
	if (movement== "back")
	{
		temp2 = position - front*cameraSpeed;
		temp = AnimationMath::lerp(position, temp2, 0.5f);
		position = temp;
	}
	if (movement == "left")
	{
		temp2 = position - right*cameraSpeed;
		temp = AnimationMath::lerp(position, temp2, 0.5f);
		position = temp;
	}
	if (movement == "right")
	{
		temp2 = position + right*cameraSpeed;
		temp = AnimationMath::lerp(position, temp2, 0.5f);
		position =temp;
	}
}

void Camera::rotateCamera(double xpos, double ypos)
{

		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}
		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos;
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.05f;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		//rotate around y axis
		yaw += xoffset;
		//rotate around x axis
		pitch += yoffset;

		//constraints
		if (pitch > 89.0f)
		{
			pitch = 89.0f;
		}
		if (pitch <-89.0f)
		{
			pitch = -89.0f;
		}

		Process();



}
void Camera::Process()
{
	glm::vec3 front1;

	front1.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front1.y = sin(glm::radians(pitch));
	front1.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(front1);
	right = glm::normalize(glm::cross(front, up));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	cameraUp= glm::normalize(glm::cross(right, front));
}
void Camera::Update()
{
	Process();
	view= glm::lookAt(position,position+front, cameraUp);
	myShader->sendUniformfMat4by4("view", view);
	myShader->sendUniformfvec3("viewPos", position);
}
void Camera::setShader(Shader *shader)
{
	myShader = shader;
}

glm::vec3 Camera::getPosition()
{
	return position;
}
void Camera::Unload()
{
	//myShader->unload();
	view = glm::mat4(1.0f);
}

Camera::~Camera()
{
	Unload();
}