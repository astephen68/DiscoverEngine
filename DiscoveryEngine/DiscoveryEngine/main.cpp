/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <map>
#include <cmath>
#include "SkinnedMesh.h"
#include "BlendedGameObject.h"

//custom classes includes
#include "FrameBuffer.h"
#include "Window.h"
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "Camera.h"
#include "Model.h"
#include "MeshLine.h"
#include "Light.h"
#include "KeyFrameController.h"
#include "GameObject.h"
#include "Animation.h"
#include "common.h"
#include "MenuState.h"
#include "AudioSystem.h"
#include "GBuffer.h"

//AUDIO SETTINGS

//settings for screen
#define SCREEN_WIDTH 800.0f
#define SCREEN_HEIGHT 600.0f

//Game time 
float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame


//global information
float globalTransX = 150.0f;
bool translate = false;
bool anReset = false;
bool first = false;
bool second = false;
bool blend = false;
//for wireframe mode
bool wireframe = false;
int toggle = 0;

//values for switching lights
bool allLights = false;
bool ambient = false;
bool specular = false;
bool specRim = false;
bool amSpRim = false;
bool diffRamp = false;
bool specRamp = false;
bool warm = true;
bool cool = false;
bool custom = false;

//values for Audio positioning
bool shouldRotate = false;
bool rollof=false;
bool resetPos = true;



//point light global locations
glm::vec3 pointLightPositions[] = {
	glm::vec3(0.0, 0.5f ,0.0f),
	glm::vec3(1.0, 0.5f ,0.0f),
	glm::vec3(0.0, 0.5f ,1.0f),
	glm::vec3(1.0, 2.0f ,0.0f)

};

//GLobal Camera
Camera *myCamera;

//callback for when window is resized, to adjust view
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

//mouse callback for mouse events
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

}
//callback for keyboard input
void processInput(GLFWwindow *window,int key, int scancode, int action, int mods)
{

	float cameraSpeed = 200.0f;
	if (key == GLFW_KEY_P && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		rollof = false;
		if (shouldRotate)
		{
			shouldRotate = false;
		}
		else
		{
			shouldRotate = true;
		}

	}
	if (key == GLFW_KEY_R && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		shouldRotate = false;
		if (rollof)
		{
			rollof = false;
		}
		else
		{
			rollof= true;
			resetPos = false;
		}

	}
	if (key == GLFW_KEY_F2 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		

		if (toggle==5)
		{
			toggle = 0;
		}
		else
		{
			toggle++;
		}
		

	}

	if (key == GLFW_KEY_1 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (allLights)
		{
			allLights = false;

			
		}
		else
		{
			allLights = true;
			ambient = false;
			specular = false;
			specRim = false;
			amSpRim = false;
		}

	}
	if (key == GLFW_KEY_2 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (ambient)
		{
			ambient = false;
			
		}
		else
		{
			ambient = true;
			allLights = false;
			specular = false;
			specRim = false;
			amSpRim = false;
		}

	}
	if (key == GLFW_KEY_3 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (specular)
		{
			specular = false;
		}
		else
		{
			specular = true;
			ambient = false;
			allLights = false;
			specRim = false;
			amSpRim = false;

		}

	}
	if (key == GLFW_KEY_4 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (specRim)
		{
			specRim = false;
		}
		else
		{
			specRim = true;
			specular = false;
			ambient = false;
			allLights = false;
			amSpRim = false;

		}

	}

	if (key == GLFW_KEY_5 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (amSpRim)
		{
			amSpRim= false;
		}
		else
		{
			amSpRim = true;
			specRim = false;
			specular = false;
			ambient = false;
			allLights = false;
	
		}

	}
	if (key == GLFW_KEY_6 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (diffRamp)
		{
			diffRamp = false;
		}
		else
		{
			diffRamp = true;
		}

	}

	if (key == GLFW_KEY_7 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (specRamp)
		{
			specRamp = false;
		}
		else
		{
			specRamp = true;
		}

	}
	if (key == GLFW_KEY_8 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (warm)
		{
			warm = false;
		}
		else
		{
			warm = true;
			cool = false;
			custom = false;
		}

	}
	if (key == GLFW_KEY_9 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (cool)
		{
			cool = false;
		}
		else
		{
			cool = true;
			warm = false;
			custom = false;
		}

	}
	if (key == GLFW_KEY_T && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (toggle)
		{
			toggle = false;
		}
		else
		{
			toggle = true;
		}
	}
	if (key == GLFW_KEY_0 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		if (custom)
		{
			custom = false;
		}
		else
		{
			custom = true;
			warm = false;
			cool= false;
		}

	}
	if (key == GLFW_KEY_R && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		anReset = true;

	}

	if (key == GLFW_KEY_1 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		first = true;
		second = false;
		blend = false;
	}

	if (key == GLFW_KEY_2 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		second = true;
		first = false;
		blend = false;

	}

	if (key == GLFW_KEY_3 && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		blend = true;
		first = false;
		second = false;
	}

	if (key == GLFW_KEY_F1 && (action == GLFW_PRESS))
	{
		wireframe = !wireframe;
		glPolygonMode(GL_FRONT_AND_BACK, wireframe ? GL_LINE : GL_FILL);
	}
	
	if (key==GLFW_KEY_W&&( action == GLFW_PRESS || action==GLFW_REPEAT))
	{
		myCamera->moveCamera(cameraSpeed*deltaTime , (std::string)"forward");
	
	}
	if (key== GLFW_KEY_S&&(action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		myCamera->moveCamera(cameraSpeed*deltaTime, (std::string)"back");
	}
	if (key== GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		myCamera->moveCamera(cameraSpeed*deltaTime, (std::string)"left");
	
	}
	if (key== GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		myCamera->moveCamera(cameraSpeed*deltaTime, (std::string)"right");
	

	}
	if ((key== GLFW_KEY_ESCAPE ||key==GLFW_KEY_Q)&&action==GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}


		
}

int main()
{


	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//_CrtSetBreakAlloc(541);
	//AUDIO SETUP
	//std::string music = "";
	//std::cout << "Enter a music file name :" << std::endl;
	//std::cin >> music;
	//music += ".wav";


	//inits the glfw libaray for use
	glfwInit();

	//init window and create window
	Window* window = DBG_NEW Window(800, 600, "Assignment 1", NULL, NULL);
	GLFWwindow* gameWindow = window->CreateGlfwWindow();

	//loads glad to manage open gl pointers, glfwgetproc loads correct function based on OS
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initalize GLAD" << std::endl;
		return -1;

	}

	//SETUP AUDIO FOR THE SCENE
	AudioSystem myAudio;
	AudioSystem::Init();
	
	myAudio.setMode(0, FMOD_3D_LINEARROLLOFF);


	// play a certain audio depending on if users option exits on repeat
	//bool result = myAudio.LoadSound("media/" + music,true,true,false);
	//if (!result)
	//{
	//	std::cout << "File cannot be loaded, default file is playing" << std::endl;
	//	myAudio.PlaySound("media/dance.wav", glm::vec3(0.0f), 0.01f);
	//}
	//else
	//{
	//	myAudio.PlaySound("media/" + music, glm::vec3(0.0f), 0.01f);
	//}


	myAudio.setMinMaxDistance(1.0f, 9.0f, 0);

	//settings for window
	glfwSetInputMode(gameWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//enables 3d texture and depth test
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_1D);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_3D);


	//setcallbacks 
	glfwSetKeyCallback(gameWindow, processInput);
	glfwSetFramebufferSizeCallback(gameWindow, framebuffer_size_callback);
	glfwSetCursorPosCallback(gameWindow, mouse_callback);
	glfwSetMouseButtonCallback(gameWindow, mouse_button_callback);


	//create shaders

	Shader* modelShader = DBG_NEW  Shader("shaders/lighting.vert", "shaders/lighting.frag");
	modelShader->createShader();

	Shader* nonMaterial = DBG_NEW  Shader("shaders/nonmaterial.vert", "shaders/nonmaterial.frag");
	nonMaterial->createShader();

	Shader *frameBuffer = DBG_NEW Shader("shaders/blur.vert", "shaders/blur.frag");
	frameBuffer->createShader();


	Shader *skyboxShader= DBG_NEW Shader("shaders/skybox.vert", "shaders/skybox.frag");
	skyboxShader->createShader();

	Shader *deferredRender = DBG_NEW Shader("shaders/nonmaterial.vert", "shaders/g_buffer.frag");
	deferredRender->createShader();

	Shader *deferredLighting = DBG_NEW Shader("shaders/deferred.vert", "shaders/deferred2.frag");
	deferredLighting->createShader();

	//create camera
	myCamera = DBG_NEW  Camera(glm::vec3(0.0f,15.0f,4.0f), glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f,0.0f,0.0f),glm::vec3(0.0f,0.0f,-1.0f), modelShader);

	float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
							 // positions   // texCoords
		-1.0f,  1.0f,0.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,0.0f,  0.0f, 0.0f,
		1.0f, -1.0f,0.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,0.0f,  0.0f, 1.0f,
		1.0f, -1.0f,0.0f,  1.0f, 0.0f,
		1.0f,  1.0f,0.0f,  1.0f, 1.0f
	};


	unsigned int quadVAO, quadVBO;
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	//create Meshs
	Mesh *cone = new Mesh();
	cone->LoadMesh("models/Cube1.obj");

	Mesh *cube = new Mesh();
	cube->LoadMesh("models/sphere.obj");
	Mesh*sphere = new Mesh();
	sphere->LoadMesh("models/Cube1.obj");
	//create models


	//create models
	Model* m = new Model(cone);
	Model* m2 = new Model(sphere);


	//light setting multiple lights for point light
	Light *myLight = new Light(modelShader, glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(1.0f,1.0f,1.0f), 
		glm::vec3(1.0f),glm::vec3(1.0f,1.0f,0.0f), 36.0f, 1.0f, 0.7f,1.8f);


	//setup  projection and model viewers defaults
	glm::mat4 projection;
	glm::mat4 view;

	glm::mat4 model=glm::mat4(1.0f);


	//set camera up for world based on texture and non textured
	modelShader->useProgram();
	modelShader->sendUniformfvec3("viewPos", myCamera->getPosition());
	projection = glm::perspective(glm::radians(45.0f), SCREEN_WIDTH / SCREEN_HEIGHT, 0.1f, 100.0f);
	//projection = glm::ortho(0.0f,  SCREEN_WIDTH/2.0f, SCREEN_HEIGHT/2.0f,0.0f, -1.0f, 100.0f);
	myCamera->setShader(modelShader);
	modelShader->sendUniformfMat4by4("projection", projection);
	modelShader->sendUniformfMat4by4("model", model);

	nonMaterial->useProgram();
	view = myCamera->getView();
	nonMaterial->sendUniformfMat4by4("view", view);
	nonMaterial->sendUniformfvec3("viewPos", myCamera->getPosition());
	myCamera->setShader(nonMaterial);
	nonMaterial->sendUniformfMat4by4("projection", projection);
	nonMaterial->sendUniformfMat4by4("model", model);

	deferredRender->useProgram();
	myCamera->setShader(deferredRender);
	view = myCamera->getView();
	deferredRender->sendUniformfMat4by4("view", view);
	deferredRender->sendUniformfvec3("viewPos", myCamera->getPosition());
	deferredRender->sendUniformfMat4by4("projection", projection);
	deferredRender->sendUniformfMat4by4("model", model);


	deferredLighting->useProgram();
	myCamera->setShader(deferredLighting);
	view = myCamera->getView();
	deferredLighting->sendUniformfMat4by4("view", view);
	deferredLighting->sendUniformfvec3("viewPos", myCamera->getPosition());
	deferredLighting->sendUniformfMat4by4("projection", projection);
	deferredLighting->sendUniformfMat4by4("model", model);


	//create textures for game 
	Texture * testTex = DBG_NEW  Texture("textures/container2.png",GL_LINEAR, GL_LINEAR,3);
	testTex->createTexture();

	Texture * testTex2 = DBG_NEW  Texture("textures/container2_specular.png", GL_LINEAR, GL_LINEAR, 4);
	testTex2->createTexture();
	//Texture *test = DBG_NEW Texture("textures/1DTexture.png",GL_CLAMP_TO_EDGE, GL_NEAREST, 1);
	//test->create1DTexture();
	//Texture *lut = DBG_NEW Texture("luts/warm.cube", GL_LINEAR,GL_LINEAR, 2);
	//lut->create3DTexture("luts/warm.cube");
	//Texture *lut2 = DBG_NEW  Texture("luts/cool.cube", GL_LINEAR, GL_LINEAR, 2);
	//lut2->create3DTexture("luts/cool.cube");
	//Texture *lut3 = DBG_NEW  Texture("luts/custom.cube", GL_LINEAR, GL_LINEAR, 2);
	//lut3->create3DTexture("luts/custom.cube");

	//Texture *cubeTexture = DBG_NEW Texture("", GL_CLAMP_TO_EDGE, GL_LINEAR, 0);
	////cubeTexture->addTexture("textures/skybox/right.jpg");
	////cubeTexture->addTexture("textures/skybox/left.jpg");
	////cubeTexture->addTexture("textures/skybox/top.jpg");
	////cubeTexture->addTexture("textures/skybox/bottom.jpg");
	////cubeTexture->addTexture("textures/skybox/front.jpg");
	////cubeTexture->addTexture("textures/skybox/back.jpg");

	//cubeTexture->addTexture("textures/sky/rightaw2.jpg");
	//cubeTexture->addTexture("textures/sky/leftaw2.jpg");
	//cubeTexture->addTexture("textures/sky/topaw2.jpg");
	//cubeTexture->addTexture("textures/sky/down.jpg");
	//cubeTexture->addTexture("textures/sky/frontaw2.jpg");
	//cubeTexture->addTexture("textures/sky/backaw2.jpg");
	//unsigned int map=cubeTexture->createCubeMap();


	
	//set game time before loop for game
	glfwSetTime(0.0);



	float reset = 0.0f;
	//WORLD SETUP


	//setup game world objects
	myAudio.SetPosition(0, glm::vec3(2.0f, 2.0f, 2.0f));
	m->setTranslation(2.0f, 2.0f, 2.0f);
	m->Update(modelShader);
	m2->setTranslation(0.0f, 0.0f, 0.0f);
	m2->Update(modelShader);


	//for cube to represent audio
	float rot = 0.0f;
	float rot2 = 0.0f;
	float distance = 0.0f;
	bool changeDirect = false;


	GBuffer *myGBuffer = new GBuffer(800, 600);
	myGBuffer->createFramebuffer();
	deferredRender->useProgram();
	deferredRender->sendInt("diffuse1", 4);
	deferredRender->sendInt("specular1", 5);

	deferredLighting->useProgram();
	deferredLighting->sendInt("gPosition", 0);
	deferredLighting->sendInt("gNormal", 1);
	deferredLighting->sendInt("gAlbedoSpec", 2);
	deferredLighting->sendInt("gSpec", 3);




	// game loop
	while (!glfwWindowShouldClose(gameWindow))
	{
		//track delta time for veolcity
		float currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//fps 
		double fps = 1.0 / deltaTime;

		glEnable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		//g buffer pass
		myGBuffer->renderFramebuffer();
		

		deferredRender->useProgram();
		myCamera->setShader(deferredRender);
		myCamera->Update();

		//CUBE

		testTex->bind(4);
		testTex2->bind(5);
		m2->Update(deferredRender);
	    m2->Draw();
		myGBuffer->unbind();
		glDisable(GL_DEPTH_TEST);
		//lighting pass
		glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
		deferredLighting->useProgram();
		myGBuffer->renderTextures();
		myCamera->setShader(deferredLighting);
		
		for (int i = 0; i < 4; i++)
		{

			//light rotation
			glm::vec4 r = glm::vec4(pointLightPositions[i].x,
				pointLightPositions[i].y, pointLightPositions[i].z, 1.0f);
			myLight->setShader(deferredLighting);
			myLight->sendLight(glm::vec3(r.x, r.y, r.z), glm::vec3(0.0f, 0.0f, -3.0f), i);
		}

		myCamera->Update();
		glBindVertexArray(quadVAO);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);

		glBindFramebuffer(GL_READ_FRAMEBUFFER, myGBuffer->getGbuffer());
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		switch (toggle)
		{
			case 0:
				glReadBuffer(GL_NONE);
				break;
			case 1:
				glReadBuffer(GL_COLOR_ATTACHMENT0);
				break;
			case 2:
				glReadBuffer(GL_COLOR_ATTACHMENT1);
				break;
			case 3:
				glReadBuffer(GL_COLOR_ATTACHMENT2);
				break;
			case 4:
				glReadBuffer(GL_COLOR_ATTACHMENT3);
				break;
		}
		glBlitFramebuffer(0, 0, 800, 600, 0, 0, 800, 600, GL_COLOR_BUFFER_BIT, GL_LINEAR);
		//glBlitFramebuffer(0, 0, 800, 600, 0, 0, 800,600, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
		

		
		window->Update();

		}


	//cleanup 
		
	//shutdown audio system ,free resources
	myAudio.Exit();
	//terminate glfw
	glfwTerminate();
	//cleanup window
	window->Unload();

	//delete myFbo;
	//delete cubeTexture;
	delete testTex;
	//delete test;
	//delete lut;
	//delete lut2;
	//delete lut3;
	delete myLight;
	delete m;
	delete m2;
	delete sphere;
	delete cone;
	delete myCamera;
	delete modelShader;
	delete nonMaterial;
	delete frameBuffer;
	delete window;
	return 0;
}

//mouse callback
void mouse_callback(GLFWwindow* window,  double xpos, double ypos)
{
	myCamera->rotateCamera(xpos, ypos);
}