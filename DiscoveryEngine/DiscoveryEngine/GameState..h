/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/

class GameState
{
public:
	virtual void Init()=0;
	virtual void Cleanup()=0;
	virtual void Pause()=0;
	virtual void Resume() = 0;

	virtual void HandleEvents() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;


};