/*Name: Stephen Richards
ID: 100458273
Date: February 11,2018
*/
#pragma once
#include <iostream>
#include <vector>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Shader.h"
#include "Camera.h"

class MeshLine
{
private:
	unsigned int vao, vbo;
	int mySize;
	float* verts;
	void setupMesh(int verticies);
public:
	MeshLine();
	MeshLine(float *verts, int verticies, int size);
	void Draw(GLenum drawType, Shader *shader, glm::mat4 proj,  Camera *myCamera,int count);


};